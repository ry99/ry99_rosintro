#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def drone_data_callback(data):
    rospy.loginfo(f"Received drone data: {data.data}")
    message = "rescue relay starts!"
    rospy.loginfo(f"Find victims:{message}")
    pub.publish(message)

def drone_monitor():
    global pub

    rospy.init_node('drone_monitor')
    pub = rospy.Publisher("victim",String,queue_size=10)
    rospy.Subscriber('drone_data', String, drone_data_callback)
    rospy.spin()

if __name__ == '__main__':
    drone_monitor()

