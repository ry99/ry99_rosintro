#!/usr/bin/bash
rosservice call /kill "turtle1"
rosservice call /spawn "x: 2.0
y: 3.0 
theta: 1.57
name: turtle1"
rosservice call /turtle1/set_pen "r: 255
g: 0
b: 0
width: 2"
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[3, 0.0, 0.0]' '[0.0, 0.0,-3.2]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[2.5, 0.0, 0.0]' '[0.0, 0.0,-2.2]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-3.5, 0.0, 0.0]' '[0.0, 0.0, 0]'
rosservice call /spawn "x: 7.0
y: 3.0 
theta: 1.57
name: turtle2"
rosservice call /turtle2/set_pen "r: 255
g: 86
b: 0
width: 2"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0.0, 0.0]' '[0.0, 0.0, -0.2]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0, 0.0, 0.0]' '[0.0, 0.0, -2.6]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[4, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-2, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -0.4]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0,-1.0, 0.0]' '[0.0, 0.0, 0.0]'
