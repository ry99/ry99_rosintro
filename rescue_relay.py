#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def survivor_info_callback(data):
    # Relay survivor info to ground teams
    rospy.loginfo(f"Relaying survivor position: {data.data}")

def rescue_relay():
    rospy.init_node('rescue_relay')
    rospy.Subscriber('drone_data', String, survivor_info_callback)
    rospy.spin()

if __name__ == '__main__':
    rescue_relay()

