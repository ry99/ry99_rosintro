#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def drone_commander():
    rospy.init_node('drone_commander')
    pub = rospy.Publisher('drone_data', String, queue_size=10)
    rate = rospy.Rate(1)  # 1 message per second
    while not rospy.is_shutdown():
        coordinates = "10.0,20.0;15.0,25.0"  # Example coordinates
        pub.publish(coordinates)
        rate.sleep()

if __name__ == '__main__':
    try:
        drone_commander()
    except rospy.ROSInterruptException:
        pass

