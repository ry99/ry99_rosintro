#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def ground_feedback_callback(data):
    # Process ground team feedback
    rospy.loginfo(f"Received ground feedback: {data.data}")

def ground_control():
    rospy.init_node('ground_control')
    rospy.Subscriber('victim', String, ground_feedback_callback)
    rospy.spin()

if __name__ == '__main__':
    ground_control()

